from optparse import OptionParser
import sys
import xlrd,xlwt

STRING = '分支行' # 按部门分离
TITLE_ROW = 0
filename = r'C:\Users\Jason\Desktop\新建文件夹\0.xlsx'

def split_file(filename):
    # 读取数据，最后获得每个独立的分支行的名称
    workbook = xlrd.open_workbook(filename)#
    sheet = workbook.sheet_by_index(0) # 通过index选择你需要分割的那个sheet    
    Title=sheet.row_values(TITLE_ROW) # 得到第一行的数据，类型为列表
    # print(Title)
    index = Title.index(STRING) # 选择所需要的那一列数据的索引
    # print(index)
    all= sheet.col_values(index)
    department = list(set(all))
    department.remove(STRING) # 删除Title这一个元素得到的是所有的部门了
    print(department)

    for sub_dt in department:
        wb_result=xlwt.Workbook()
        row_i =0 
        sheet_subdt=wb_result.add_sheet(sub_dt,cell_overwrite_ok=True)
        # 以下两句复制标题行
        for j in range(sheet.ncols):
            sheet_subdt.write(row_i,j,sheet.row_values(TITLE_ROW)[j])
        # 第二行开始复制数据行
        row_i += 1
        for i in range(1,sheet.nrows): #第1行是Titile，从第2行开始
            if sheet.row_values(i)[index] == sub_dt:
                for j in range(sheet.ncols):
                    sheet_subdt.write(row_i,j,sheet.row_values(i)[j])
                row_i += 1 # 只有当值为sub_dt时才计数，如果不相等，则不计数
        wb_result.save(f'{sub_dt}.xls')   
        
        
if __name__=='__main__':
    split_file(filename)